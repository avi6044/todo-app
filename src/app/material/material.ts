import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { DragDropModule } from "@angular/cdk/drag-drop";
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';

const modules = [
    CommonModule,
    DragDropModule,
    MatDialogModule,
    MatIconModule,
    MatSnackBarModule
]

@NgModule({
    imports:[
        ...modules
    ],
    exports:[
        ...modules
    ]
})
export class MaterialModule {}