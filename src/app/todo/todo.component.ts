import { Component, OnInit, TemplateRef } from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';

export interface TaskList {
  id:string,
  title:string,
  description:string,
  date:Date;
}

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {

  todo: Array<TaskList> = [];
  done: Array<TaskList> = [];
  @ViewChild('addTodoModel') todoAddModel: TemplateRef<any>;
  @ViewChild('deleteTodoModel') deleteModal: TemplateRef<any>;

  currentTitle:string = "Add New Todo";
  error:string = "";
  modalRef:MatDialogRef<any>;
  isError:boolean = false;


  todoForm =  new FormGroup({
    title: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required)
  })

  horizontalPosition: MatSnackBarHorizontalPosition = 'right';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(private matDialog:MatDialog,
    private matSnackBarService:MatSnackBar) {}

  ngOnInit() {
    this.todoForm.valueChanges.subscribe(res => {
       this.error = "";
    })
  }

  /**
   * Drag and Drop function in this function User drag 
   * the data into the complete list or 
   * either remove into the todo list
   * @param event 
   */
  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
  }

  /**
   * open popup 
   * @param currentData 
   */
  openAddTodoModal(currentData?:any){
    this.error = "";
    const data = currentData ? currentData : '';
    this.currentTitle = !currentData ? 'Add New Todo' : 'Edit Todo';
    if(data) {
        this.todoForm.patchValue({
          title: data.title,
          description: data.description
        })
    }
    const ref =  this.modalRef = this.matDialog.open(this.todoAddModel,{
      width:"50%",      
      data:data,
      panelClass:"add-todo"
    })

    ref.afterClosed().subscribe(res => {
       if(res && res ==  'cancel') {
         this.todoForm.reset();
       }
    })
  }

  /**
   * add data into the todo list 
   */
  addTodoTask() {
    const data =  this.checkToDoList(); 
    if(!this.error) {
      data.id = this.guidGenerator();
      data.title.trim();
      this.todo.push(data);
      this.todoForm.reset();
      this.modalRef.close();
      this.matSnackBarService.open("Added successfully",'',{
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      duration:1000
      })
    }
  }

  /**
   * Edit data from the specific todo list
   * @param currentData 
   */
  editData(currentData) {
    const data =  this.checkToDoList(currentData); 
    if(!this.isError) {
        this.todo.filter(item => {
          if(item.id === currentData.id) {
            item.title = data.title.trim();
            item.date = new Date();
            item.description = data.description;
            this.todoForm.reset();
            this.modalRef.close();
            this.matSnackBarService.open("Edit successfully",'',{
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
              duration:1000
              })
          }
      })
    }

  }

  /**
   * Check todo list if Exit the show error 
   * @param data 
   * @returns 
   */
  checkToDoList(data?) {
    let addData:any =  this.todoForm.value;
    addData.date =  new Date();
    let found = this.todo.find( item => item.title.toLowerCase().trim() === addData.title.toLowerCase().trim());
    
    if(found) {
      if(data && data.title.toLowerCase().trim() === addData.title.toLowerCase().trim()) {
        this.isError = false;
      } else {
        this.isError = true;
      }
      this.error = "Please Title enter different title name"
    } else {
      this.isError = false;
    }
   
    return addData;
  }

  /**
   * Delete model open and if yes the remove 
   * the data from the list
   * @param data 
   */
  openDeleteTodoModel(data) {
    const ref =  this.matDialog.open(this.deleteModal,{
      width:"40%",
      data:data,
      panelClass:"delete-todo"
    })
    ref.afterClosed().subscribe(res => {
       if(res) {
           const INDEX =  this.todo.findIndex(item => item.id === data.id)
           if(INDEX > -1) {
             this.todo.splice(INDEX,1);
             this.matSnackBarService.open("Deleted successfully",'',{
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
              duration:1000
              })
           }
       }
    })
  }

  /**
   * Generate  Random Uid
   * @returns  uid String
   */
  guidGenerator() {
    var S4 = function() {
       return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
    };
    return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
  }

}
